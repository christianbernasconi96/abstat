package com.repository;

import java.util.List;

import org.springframework.data.solr.repository.SolrCrudRepository;

import com.model.ResourceSolr;

public interface ResourceRepo extends SolrCrudRepository<ResourceSolr, Long>{

	void deleteBySummary(String summary_id);

	List<ResourceSolr> findBySummary(String summary);
}
