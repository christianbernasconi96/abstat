package com.repository;

import java.util.List;

import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Component;

import com.model.AKPSolr;


@Component
public interface AKPRepo extends SolrCrudRepository<AKPSolr, Long>{

	void deleteBySummary(String summary_id);

	List<AKPSolr> findBySummary(String summary);
}
