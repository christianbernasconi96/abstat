package com.service;


public interface TriplesExtractionService {
	
	public String extractGroupedTriples(String subj, String pred, String obj, String dataset, String ontology,
			String akpType, String cardinalityType, Integer predictedCardinality, Integer limit, Integer offset,
			boolean sort, boolean detailed);
	
	public String extractSingleTriples(String subj, String pred, String obj, String dataset, String ontology,
			String akpType, String cardinalityType, Integer limit, Integer offset);
	
}
