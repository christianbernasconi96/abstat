package com.service;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("single-machine")
public class FSReader implements FileReader{

	private LineIterator lines;

	@Override
	public void open(String path) throws Exception {
		this.lines = IOUtils.lineIterator(FileUtils.openInputStream(new File(path)), "UTF-8");
	}
	
	@Override
	public String[] nextLine() {
		String line =  lines.nextLine();
		return line.split("##");
	}

	@Override
	public boolean hasNextLine() throws Exception{
		return lines.hasNext();
	}

}
