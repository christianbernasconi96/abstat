package com.service;

public interface FileReader {

	public String[] nextLine();
	
	boolean hasNextLine() throws Exception;

	void open(String path) throws Exception;
}
