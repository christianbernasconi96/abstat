package com.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.model.Dataset;
import com.model.SubmitConfig;

@Service
@Profile("spark")
public class SummarizationServiceSparkImpl implements SummarizationService {

	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	EmailService emailService;

	
	@Async("processExecutor")
	public void summarizeAsyncWrapper(SubmitConfig subCfg, String email) {
		summarize(subCfg, email);
	}
	

	public void summarize(SubmitConfig subCfg, String email) {
		try {
			Dataset dataset = datasetService.findDatasetById(subCfg.getDsId());
			String ontId = subCfg.getListOntId().get(0);
			String ontPath = ontologyService.findOntologyById(ontId).getPath();

			// spark job submission
			sparkSubmit(dataset.getPath(), ontPath, subCfg.getSummaryPath());

			// save configuration
			submitConfigService.add(subCfg);

			// mail notification
			emailService.sendMail(email, "OK");

		} catch (Exception e) {
			e.printStackTrace();
			emailService.sendMail(email, "error");
		}
	}

	
	private void sparkSubmit(String dataset, String ontology, String output_dir) throws Exception {
		String[] cmd = { "/bin/bash", "abstat-distributed/submit-job.sh", dataset, ontology, output_dir };
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.redirectErrorStream(true);
		pb.directory(new File(System.getProperty("user.dir")).getParentFile());
		Process p = pb.start();

		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		boolean done = false;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
			if(line.contains("distributed summarization done"))
				done = true;
		}
		if(!done)
			throw new Exception("submit-job.sh exit with error");
	}

}
