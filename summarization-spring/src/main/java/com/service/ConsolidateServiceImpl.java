package com.service;

import java.io.BufferedReader;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.AKP;
import com.model.AKPSolr;
import com.model.Resource;
import com.model.ResourceSolr;
import com.model.SubmitConfig;
import com.repository.AKPRepo;
import com.repository.ResourceRepo;
import com.summarization.ontology.RDFResource;
import com.summarization.ontology.TypeOf;

@Service
public class ConsolidateServiceImpl implements ConsolidateService{
	@Autowired
	OntologyService ontologyService;
	@Autowired
	ResourceService resourceService;
	@Autowired
	AKPService AKPService;
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	AKPRepo akpRepo;
	@Autowired
	ResourceRepo resRepo;
	@Autowired
	FileReader fileReader;
	
	static final int BufferSize = 1000000;
	HashMap<String, Long> datatypes;
	HashMap<String, Long> concepts;
	HashMap<String, Long> datatype_properties;
	HashMap<String, Long> object_properties;
	File countConcepts;
	File countDatatype;
	File countObjectProperties;
	File countDatatypeProperties;
	String id_current_summary;
	String domain;
	String dataset;

	@Override
	public void init(String dataset, String domain, String id_current_summary, String countConcepts, String countDatatype, String countObjectProperties, String countDatatypeProperties) {
		this.dataset = dataset;
		this.domain = domain;
		this.id_current_summary = id_current_summary;
		this.countConcepts = new File(countConcepts);
		this.countDatatype = new File(countDatatype);
		this.countObjectProperties = new File(countObjectProperties);
		this.countDatatypeProperties = new File(countDatatypeProperties);
	}
	
	
	@Override
	public void loadResources(String path, SubmitConfig config, String type) throws Exception{
		TypeOf typeOf = new TypeOf(domain);
		ArrayList<Resource> batch = new ArrayList<Resource>();
		int count = BufferSize;
		
		List<String> ontList = new ArrayList<String>();
		for(String id : config.getListOntId()) {
			String ontology = ontologyService.findOntologyById(id).getName();
			ontList.add(ontology);
		}
		
		fileReader.open(path);
		while (fileReader.hasNextLine()) {
			String[] splitted = fileReader.nextLine();
			String globalResource = splitted[0];
			String frequency = splitted[1];
			
			com.model.Resource resource = new com.model.Resource();

			resource.setId(DigestUtils.md5Hex(StringUtils.join(config.getId(), globalResource, type)));
			resource.setSeeAlso(globalResource);
			resource.setType(type);
			resource.setSubType(typeOf.resource(globalResource));
			resource.setDatasetOfOrigin(dataset);
			resource.setOntologiesOfOrigin(ontList);
			resource.setSummary_conf(config.getId());
			resource.setFrequency(Long.parseLong(frequency));
			
			if(count>0) {
				batch.add(resource);
				count--;
			}
			else {
				count = BufferSize;
				resourceService.add(batch);
				batch = new ArrayList<Resource>();
			}
			
		}
		resourceService.add(batch);
		batch = null;
	}
	
	
	@Override
	public void loadAKPs(String path, SubmitConfig config, String type) throws Exception{
		TypeOf typeOf = new TypeOf(domain);
		ArrayList<AKP> batch = new ArrayList<AKP>();
		int count = BufferSize;
		
		List<String> ontList = new ArrayList<String>();
		for(String id : config.getListOntId()) {
			String ontology = ontologyService.findOntologyById(id).getName();
			ontList.add(ontology);
		}
		
		fileReader.open(path);
		while (fileReader.hasNextLine()) {
				String[] splitted = fileReader.nextLine();
				String globalSubject = splitted[0];
				String globalPredicate = splitted[1];
				String globalObject = splitted[2];
				String frequency = splitted[3];

				com.model.AKP AKP = new com.model.AKP();

				String internal = null;
				switch(type) {
				case "Object AKP":
					internal = typeOf.objectAKP(globalSubject, globalObject);
					break;
				case "Datatype AKP":
					internal = typeOf.datatypeAKP(globalSubject);
					break;
				default:
					AKP.setType("NONE");
				}

				AKP.setId(DigestUtils.md5Hex(StringUtils.join(config.getId(), globalSubject, globalPredicate, globalObject)));
				AKP.setPatternType("minimal");
				AKP.setType(type);
				AKP.setSubType(internal);
				AKP.setSubject(globalSubject);
				AKP.setPredicate(globalPredicate);
				AKP.setObject(globalObject);
				AKP.setFrequency(Long.parseLong(frequency));
				AKP.setDatasetOfOrigin(dataset);
				AKP.setOntologiesOfOrigin(ontList);
				AKP.setSummary_conf(config.getId());
				AKP.setStatus("not validated");
				
				if(count>0) {
					batch.add(AKP);
					count--;
				}
				else {
					batch.add(AKP);
					AKPService.add(batch);
					count = BufferSize;
					batch = new ArrayList<AKP>();
				}
			
		}
		AKPService.add(batch);
		batch = null;
	}
	
	
	@Override
	public void loadNumberOfInstances(String path, SubmitConfig config, String type) throws Exception {
		fileReader.open(path);
		while (fileReader.hasNextLine()) {
			String[] splitted = fileReader.nextLine();
			if (Integer.parseInt(splitted[3]) > 0) {
				AKP akp = AKPService.getAKP(splitted[0], splitted[1], splitted[2], config.getId());

				if (akp != null) { // to deal with no-minimal patterns
					akp.setNumberOfInstances(Long.parseLong(splitted[4]));
					AKPService.update(akp);
				}
			}

		}
	}
	

	@Override
	public void loadPatternCardinalities(String path,SubmitConfig config) throws Exception {
		fileReader.open(path);
		while (fileReader.hasNextLine()) {
				String[] parts = fileReader.nextLine();					
				AKP akp = AKPService.getAKP(parts[0], parts[1], parts[2], config.getId());	

				akp.setCardinality1(Long.parseLong(parts[3]));
				akp.setCardinality2(Long.parseLong(parts[4]));
				akp.setCardinality3(Long.parseLong(parts[5]));
				akp.setCardinality4(Long.parseLong(parts[6]));
				akp.setCardinality5(Long.parseLong(parts[7]));
				akp.setCardinality6(Long.parseLong(parts[8]));
				
				if(config.isRichCardinalities()) {
					String[] subjsObj = parts[9].substring(2,parts[9].length()-2).split(", ");
					String[] subjObjs = parts[10].substring(2,parts[10].length()-2).split(", ");
					Map<Long, Long> subjsObjMap = new HashMap<Long,Long>();
					Map<Long, Long> subjObjsMap = new HashMap<Long,Long>();
					
					for(String keyvalue : subjsObj) {
						String[] splitted = keyvalue.split("=");
						subjsObjMap.put(Long.parseLong(splitted[0]), Long.parseLong(splitted[1]));	
					}
					for(String keyvalue : subjObjs) {
						String[] splitted = keyvalue.split("=");
						subjObjsMap.put(Long.parseLong(splitted[0]), Long.parseLong(splitted[1]));	
					}
					akp.setSubjsObj(subjsObjMap);
					akp.setSubjObjs(subjObjsMap);
				}

				AKPService.update(akp);
			
		}
	}
	
	
	@Override
	public void loadPredictedCardinalities(String path, SubmitConfig config) throws Exception {
		fileReader.open(path);
		while (fileReader.hasNextLine()) {
			String[] parts = fileReader.nextLine();
			AKP akp = AKPService.getAKP(parts[0], parts[1], parts[2], config.getId());

			boolean violatedSubjsObj = akp.getCardinality1() > Long.parseLong(parts[3]);
			boolean violatedSubjObjs = akp.getCardinality4() > Long.parseLong(parts[4]);
			String status = violatedSubjObjs || violatedSubjsObj ? "invalid" : "valid";
			akp.setStatus(status);

			AKPService.update(akp);
		}
	}


	@Override
	public void indexSummary(String path, String type) throws Exception {
		ArrayList<ResourceSolr> buffer = new ArrayList<ResourceSolr>();
		TypeOf typeOf = new TypeOf(domain);
		fileReader.open(path);
		while(fileReader.hasNextLine()){
			String[] parts = fileReader.nextLine();
			ResourceSolr res = new ResourceSolr();
			res.setType(type);
			res.setDataset(dataset);
			res.setSummary(this.id_current_summary);
			if(type.contains("Akp")){
				String subject = parts[0];
				String subjectLocalName = new RDFResource(subject).localName();
				String property = parts[1];
				String propertyLocalName = new RDFResource(property).localName();
				String object = parts[2];
				String objectLocalName = new RDFResource(object).localName();
				Long occurrences = Long.parseLong(parts[3]);
				String subtype = "";

				if(type.equals("datatypeAkp")) 
					subtype =  typeOf.datatypeAKP(subject);
				else
					subtype =  typeOf.objectAKP(subject, object);
				
				res.setURI(new String[]{subject, property, object});
				res.setSubtype(subtype);
				res.setFullTextSearchField(new String[]{subjectLocalName, propertyLocalName, objectLocalName});
				res.setOccurrence(occurrences);
			}
			else {
				String resource = parts[0];
				String localName = new RDFResource(resource).localName();
				Long occurrences = Long.parseLong(parts[1]);
				String subtype = typeOf.resource(resource);
				
				res.setURI(new String[]{resource});
				res.setSubtype(subtype);
				res.setFullTextSearchField(new String[]{localName});
				res.setOccurrence(occurrences);
			}
			if(buffer.size() >= BufferSize) {
				resRepo.save(buffer);
				buffer.clear();
			}
			buffer.add(res);
		}
		if(!buffer.isEmpty())
			resRepo.save(buffer);
	}


	@Override
	public void indexAKPsAutocomplete(String path, String type) throws Exception {
		if(this.datatypes==null && this.concepts==null) 
			frequenciesReader();
		
		TypeOf typeOf = new TypeOf(domain);
		ArrayList<AKPSolr> buffer = new ArrayList<AKPSolr>();
		fileReader.open(path);
		while(fileReader.hasNextLine()){
			String[] parts = fileReader.nextLine();
			String subject = parts[0];
			String subjectLocalName = new RDFResource(subject).localName();
			String property = parts[1];
			String propertyLocalName = new RDFResource(property).localName();
			String object = parts[2];
			String objectLocalName = new RDFResource(object).localName();
			Long occurrences = Long.parseLong(parts[3]);
			String subtype = "";
			
			if(type.equals("datatypeAkp")) 
				subtype =  typeOf.datatypeAKP(subject);
			else 
				subtype =  typeOf.objectAKP(subject, object);

			
			AKPSolr akp = new AKPSolr();
			akp.setURI(new String[]{
					subject, property, object
			});
			akp.setType(type);
			akp.setDataset(dataset);
			akp.setSummary(id_current_summary);
			akp.setSubtype(subtype);
			akp.setFullTextSearchField(new String[]{
					subjectLocalName, propertyLocalName, objectLocalName
			});
			akp.setSubject(subject);
			akp.setPredicate(property);
			akp.setObject(object);
			akp.setSubject_ngram(subjectLocalName);
			akp.setPredicate_ngram(propertyLocalName);
			akp.setObject_ngram(objectLocalName);
			akp.setOCcurrence(occurrences);
			
			if(concepts.containsKey(subject))                     // questo if è necessario perchè i concetti esterni non sono elecnati in count-concepts.txt e quidni non sono nella collection concepts
				akp.setSubjectFreq(concepts.get(subject));
			else
				akp.setSubjectFreq(0);
			
			if(type.equals("datatypeAkp")){
				akp.setPredicateFreq(datatype_properties.get(property));
				akp.setObjectFreq(datatypes.get(object));
			}
			else{
				akp.setPredicateFreq(object_properties.get(property));
				if(concepts.containsKey(object))                     // questo if è necessario perchè i concetti esterni non sono elecnati in count-concepts.txt e quidni non sono nella collection concepts
					akp.setObjectFreq(concepts.get(object));
				else
					akp.setObjectFreq(0);
			}
			
			akp.setSubject_plus_dataset(subject + "_" + dataset);
			akp.setPredicate_plus_dataset(property + "_" + dataset);
			akp.setObject_plus_dataset(object + "_" + dataset);
			
			if(buffer.size() >= BufferSize) {
				akpRepo.save(buffer);
				buffer.clear();
			}
			buffer.add(akp);	
		}
		if(!buffer.isEmpty())
			akpRepo.save(buffer);
	}

	
	public void frequenciesReader() {
			this.datatypes = getFreqs(countDatatype);
			this.concepts = getFreqs(countConcepts);
			this.datatype_properties = getFreqs(countDatatypeProperties);
			this.object_properties = getFreqs(countObjectProperties);
	}

	
	// Extracts frequencies from the input file
	public HashMap<String, Long> getFreqs(File file) {
		HashMap<String, Long> map = new HashMap<String, Long>();
		try {
			BufferedReader br = new BufferedReader(new java.io.FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				if (!line.equals("")) {
					String[] splitted = line.split("##");
					String key = splitted[0];
					Long value = Long.parseLong(splitted[1]);
					map.put(key, value);
				}
			}
			br.close();
		} catch (Exception e) {
		}
		return map;
	}

}

