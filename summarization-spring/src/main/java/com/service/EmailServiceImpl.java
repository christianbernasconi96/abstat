package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	JavaMailSender emailSender;

	
	public void sendMail(String address, String status) {
		if (address != null && !address.equals("")) {
			String text = "";
			if (status.equals("OK"))
				text = "Dear user, \n\n" + "A summarization job that you have run on ABSTAT is now completed. \n"
						+ "You can now check and consolidate the summary using the ABSTAT website at http://backend.abstat.disco.unimib.it \n\n"
						+ "The ABSTAT Summarization Framework \n\n" + "\n"
						+ "ABSTAT 1.0 // Licensed under Creative Commons - GNU Affero General Public License v3.010.";
			else
				text = "Dear user, \n\n"
						+ "The summarization job that you have run on ABSTAT has encountered a problem. Please contact support.\n\n"
						+ "The ABSTAT Summarization Framework \n\n" + "\n"
						+ "ABSTAT 1.0 // Licensed under Creative Commons - GNU Affero General Public License v3.010.";

			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(address);
			if (status.equals("OK"))
				message.setSubject("Your summary is ready!");
			else
				message.setSubject("Oops!");
			message.setText(text);
			emailSender.send(message);
		}
	}

}
