package com.service;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.code.externalsorting.ExternalSort;
import com.model.Dataset;
import com.model.SubmitConfig;
import com.summarization.dataset.ParallelProcessing;
import com.summarization.dataset.ParallelSplitting;
import com.summarization.export.AggregateConceptCounts;
import com.summarization.export.CalculateMinimalTypes;
import com.summarization.export.CalculatePropertyMinimalization;
import com.summarization.export.DatatypeSplittedPatternInference;
import com.summarization.export.GenerateShacl;
import com.summarization.export.MainCardinality;
import com.summarization.export.ObjectSplittedPatternInference;
import com.summarization.export.ProcessDatatypeRelationAssertions;
import com.summarization.export.ProcessObjectRelationAssertions;
import com.summarization.export.ProcessOntology;

@Service
@Profile("single-machine")
public class SummarizationServiceSMImpl implements SummarizationService {

	private static final int nCores = Runtime.getRuntime().availableProcessors();
	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	EmailService emailService;
	
	
	@Async("processExecutor")
	public void summarizeAsyncWrapper(SubmitConfig subCfg, String email)   {
		summarize(subCfg, email);
	}


	public void summarize(SubmitConfig subCfg, String email) {
		try {
			String dsId = subCfg.getDsId();
			Dataset dataset = datasetService.findDatasetById(dsId);
			String datasetName = dataset.getName();
			String ontId = subCfg.getListOntId().get(0);
			String ontPath = new File(ontologyService.findOntologyById(ontId).getPath()).getCanonicalPath();
			String ontName = subCfg.getListOntNames().get(0);
			String summary_dir = subCfg.getSummaryPath();

			String datasetSupportFileDirectory = summary_dir + "/reports/tmp-data-for-computation/";
			checkFile(summary_dir);
			checkFile(datasetSupportFileDirectory);

			// Process ontology
			System.out.println("\"------------ ONTOLOGY PROCESSING --------------------");
			ProcessOntology.main(new String[] { ontPath, datasetSupportFileDirectory });			

			// Dataset splitting
			if(!dataset.isSplit()) {
				String datasetPath = dataset.getPath();
				String tempSplitDir = datasetPath.substring(0, datasetPath.lastIndexOf('/')) + "/organized-splitted-deduplicated-tmp-file";
				String splitDir = datasetPath.substring(0, datasetPath.lastIndexOf('/')) + "/organized-splitted-deduplicated";
				split(datasetPath, tempSplitDir, splitDir);
				dataset.setSplit(true);
				datasetService.update(dataset);
			}

			String minTypeResult = summary_dir + "/min-types/min-type-results/";
			String patternsPath = summary_dir + "/patterns/";
			String shaclPath = patternsPath + "/shacl/";
			String typesDirectory = "../data/DsAndOnt/dataset/" + datasetName + "/organized-splitted-deduplicated/";
			checkFile(minTypeResult);
			checkFile(patternsPath);

			// core summarization
			coreSummarization(ontPath, typesDirectory, minTypeResult, patternsPath);

			Path objectAkpGrezzo_target = Paths.get(patternsPath + "object-akp_grezzo.txt");
			Path datatypeAkpGrezzo_target = Paths.get(patternsPath + "datatype-akp_grezzo.txt");

			// Property minimalization
			if (subCfg.isPropertyMinimaliz())
				propertyMinimalization(objectAkpGrezzo_target, datatypeAkpGrezzo_target, patternsPath, ontPath);
			// Inferenze
			if (subCfg.isInferences())
				inference(patternsPath, ontPath);
			// Cardinalità
			if (subCfg.isCardinalita())
				cardinality(patternsPath, subCfg.isRichCardinalities());
			// Shacl validation
			if(subCfg.isShaclValidation())
				shaclValidation(dataset.getPath(), datasetName, ontPath, ontName, patternsPath, shaclPath);

			// save configuration
			submitConfigService.add(subCfg);

			// mail notification
			emailService.sendMail(email, "OK");
			
		} catch (Exception e) {
			e.printStackTrace();
			emailService.sendMail(email, "error");
		}
	}	


	private void shaclValidation(String datasetPath, String datasetName, String ontologyPath, String ontologyName, String patternsPath, String shaclPath) throws IOException {

		System.out.println("------------ SHACL VALIDATION --------------------");

		// setting absolute paths
		String currentPath = System.getProperty("user.dir");
		datasetPath = currentPath + "/" + datasetPath;
		patternsPath = currentPath+ "/" + patternsPath;
		shaclPath = currentPath + "/" + shaclPath;

		// preparing bulk load parameters
		String datasetDirPath = datasetPath.substring(0, datasetPath.lastIndexOf("/"));
		String datasetFilename = datasetPath.replace(datasetDirPath + "/", "");
		String ontologyDirPath = ontologyPath.substring(0, ontologyPath.lastIndexOf("/"));
		String ontologyFilename = ontologyPath.replace(ontologyDirPath + "/", "");

		// TODO: stabilire graphName
		System.out.println("Loading dataset in Virtuoso ----------------------------------------------------------------");
		runVirtuosoBulkLoad(datasetDirPath, datasetFilename, datasetName);

		// TODO: stabilire graphName e ruleName
		System.out.println("Loading ontology in Virtuoso ----------------------------------------------------------------");
		runVirtuosoBulkLoad(ontologyDirPath, ontologyFilename, ontologyName);
		// setting inference rule
		runVirtuosoRuleSet(ontologyName, ontologyName);

		// TODO: mettere vera previsione
		System.out.println("Predicting cardinalities ----------------------------------------------------------------");
		predictCardinalities(patternsPath);

		System.out.println("Generating shapes  ----------------------------------------------------------------");
		String shapesPath = shaclPath + "/shapes";
		//		ShaclGenerator.generateShacl(patternsPath, shapesPath);
		GenerateShacl.main(new String[] {patternsPath, shapesPath});

		// TODO: stabilire graphName
		System.out.println("Loading shapes in Virtuoso ----------------------------------------------------------------");
		runVirtuosoBulkLoad(shapesPath, "*.ttl", datasetName + "-shapes");


	}



	private void predictCardinalities(String patternsPath) throws IOException {
		String line; 
		String[] splittedLine;
		String akp;
		int maxSubjsObj;
		int avgSubjsObj;
		int maxSubjObjs;
		int avgSubjObjs;
		int maxSubjsObjPredicted;
		int maxSubjObjsPredicted;

		// input
		Scanner patternCardinalities = new Scanner(new File(patternsPath + "/patternCardinalities.txt"));
		// output
		PrintWriter pw = new PrintWriter(new FileWriter(patternsPath + "/predictedCardinalities.txt"));

		while (patternCardinalities.hasNextLine()) {
			// line = subject##property##object## maxSubjsObjPredicted-maxSubjObjsPredicted
			line = patternCardinalities.nextLine(); 

			splittedLine = line.split("##");

			akp = splittedLine[0] + "##" + splittedLine[1] + "##" + splittedLine[2];

			maxSubjsObj = Integer.parseInt(splittedLine[3]);
			avgSubjsObj = Integer.parseInt(splittedLine[4]);
			maxSubjObjs = Integer.parseInt(splittedLine[6]);
			avgSubjObjs = Integer.parseInt(splittedLine[7]);

			// prediction
			maxSubjsObjPredicted = maxSubjsObj > 2 * avgSubjsObj ? 2 * avgSubjsObj : maxSubjsObj;
			maxSubjObjsPredicted = maxSubjObjs > 2 * avgSubjObjs ? 2 * avgSubjObjs : maxSubjObjs;

			pw.println(akp + "##" + maxSubjsObjPredicted + "##" + maxSubjObjsPredicted);
			// pw.flush();
		}

		patternCardinalities.close();
		pw.close();
	}

	private void runVirtuosoBulkLoad(String datasetDirPath, String filenamePattern, String graphName) throws IOException {
		System.out.println("Running virtuoso_bulk_load.sh with parameters:");
		System.out.println("$1 = " + datasetDirPath);
		System.out.println("$2 = " + filenamePattern);
		System.out.println("$3 = " + graphName);
		ProcessBuilder pb = new ProcessBuilder("./virtuoso_bulk_load.sh", datasetDirPath, filenamePattern, graphName);
		Process p = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null)
			System.out.println(line);
	}

	private void runVirtuosoRuleSet(String ruleName, String ontologyName) throws IOException {
		System.out.println("Running virtuoso_rule_set.sh with parameters:");
		System.out.println("$1 = " + ruleName);
		System.out.println("$2 = " + ontologyName);

		ProcessBuilder pb = new ProcessBuilder("./virtuoso_rule_set.sh", ruleName, ontologyName);
		Process p = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null)
			System.out.println(line);
	}


	private void coreSummarization(String ontPath, String typesDirectory, String minTypeResult, String patternsPath) throws Exception {
		System.out.println("------------ MINIMAL TYPES CALCULATION --------------------");
		// Minimal types
		CalculateMinimalTypes.main(new String[] { ontPath, typesDirectory, minTypeResult });
		// Aggregate concepts
		AggregateConceptCounts.main(new String[] { minTypeResult, patternsPath });
		// Process dt relational asserts
		System.out.println("------------ DATATYPE AKP CALCULATION ------------ ");
		ProcessDatatypeRelationAssertions.main(new String[] { typesDirectory, minTypeResult, patternsPath });
		// Process obj relational asserts
		System.out.println("------------ OBJECT AKP CALCULATION ------------ ");
		ProcessObjectRelationAssertions.main(new String[] { typesDirectory, minTypeResult, patternsPath });

		// sposto i file akp_grezzo
		Path objectAkpGrezzo_target = Paths.get(patternsPath + "object-akp_grezzo.txt");
		Path datatypeAkpGrezzo_target = Paths.get(patternsPath + "datatype-akp_grezzo.txt");
		Path datatypeAkpGrezzo_src = Paths.get("datatype-akp_grezzo.txt");
		Path objectAkpGrezzo_src = Paths.get("object-akp_grezzo.txt");

		if (!datatypeAkpGrezzo_src.toFile().exists())
			datatypeAkpGrezzo_src.toFile().createNewFile();
		if (objectAkpGrezzo_src.toFile().exists())
			objectAkpGrezzo_src.toFile().createNewFile();
		Files.move(datatypeAkpGrezzo_src, datatypeAkpGrezzo_target, REPLACE_EXISTING);
		Files.move(objectAkpGrezzo_src, objectAkpGrezzo_target, REPLACE_EXISTING);
	}

	private void split(String datasetPath, String tempSplitDir, String splitDir) throws Exception {
		checkFile(tempSplitDir);
		checkFile(splitDir);

		chunk(datasetPath, tempSplitDir);

		System.out.println("\nSTART parallel splitting ---------------------------------------------------------------- ");
		Instant nowSplit = Instant.now();
		ParallelSplitting splitterFasullo = new ParallelSplitting(new File(splitDir));
		new ParallelProcessing(new File(tempSplitDir)).splittingProcess(splitterFasullo, nCores);
		System.out.println("END parallel splitting: " + Duration.between(nowSplit, Instant.now()).getSeconds() +"s -----------------------------------------------------------"  );

		System.out.println("\nSTART  sorting ----------------------------------------------------------------");
		Instant nowSort = Instant.now();
		sort(new File(splitDir));
		System.out.println("END sorting: " + Duration.between(nowSort, Instant.now()).getSeconds() + "s -----------------------------------------------------------" );
	}


	private void chunk(String datasetPath, String tempSplitDir) throws Exception {
		Instant nowSort = Instant.now();
		System.out.println("START chuncking");
		System.out.println("counting dataset lines");
		LineNumberReader reader = new LineNumberReader(new FileReader(datasetPath));
		while ((reader.readLine()) != null);
		int lines = reader.getLineNumber();
		reader.close();

		BufferedWriter[] writers = new BufferedWriter[nCores];
		for (int i = 0; i < nCores; i++)
			writers[i] = new BufferedWriter(new PrintWriter(tempSplitDir + "/part_" + i));

		BufferedReader br = new BufferedReader(new FileReader(datasetPath));
		String line;
		int count = lines / nCores;
		int i = 0;
		while ((line = br.readLine()) != null && !line.equals("")) {
			if (count == 0 && i < writers.length - 1) {
				writers[i].close();
				i++;
				count = lines / nCores;
			}
			writers[i].write(line + "\n");
			count--;
		}
		writers[nCores - 1].close();
		br.close();

		System.out.println("END chuncking : " + Duration.between(nowSort, Instant.now()).getSeconds() + "s");
	}


	private void sort(File splitDir) throws Exception {
		for( File f : splitDir.listFiles()) {
			Instant now = Instant.now();
			String cmd = "sort -u " + f.getAbsolutePath() + " -o " + f.getAbsolutePath();
			System.out.println("START " + f.getName());
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = reader.readLine()) != null)
				System.out.println(line);
			p.waitFor();

			System.out.println(f.getName() + ": " + Duration.between(now, Instant.now()).getSeconds() +"s");
		}
	}


	private void propertyMinimalization(Path objectAkpGrezzo_target, Path datatypeAkpGrezzo_target, String patternsPath, String ontPath) throws Exception{
		System.out.println("\"------------ PROPERTY MINIMALIZATION --------------------");
		//ordino i file akp_grezzo
		ExternalSort.mergeSortedFiles(ExternalSort.sortInBatch(new File(objectAkpGrezzo_target.toString())), new File(patternsPath+"object-akp-grezzo_Sorted.txt"));
		ExternalSort.mergeSortedFiles(ExternalSort.sortInBatch(new File(datatypeAkpGrezzo_target.toString())), new File(patternsPath+"datatype-akp-grezzo_Sorted.txt"));
		//rinomino i file ordinati
		Path datatypeAkpGrezzo_sorted = Paths.get(patternsPath+"datatype-akp-grezzo_Sorted.txt");
		Path objectAkpGrezzo_sorted = Paths.get(patternsPath + "object-akp-grezzo_Sorted.txt");
		Files.move(datatypeAkpGrezzo_sorted, datatypeAkpGrezzo_sorted.resolveSibling("datatype-akp_grezzo.txt"), REPLACE_EXISTING);
		Files.move(objectAkpGrezzo_sorted, objectAkpGrezzo_sorted.resolveSibling("object-akp_grezzo.txt"), REPLACE_EXISTING);
		CalculatePropertyMinimalization.main(new String[] {ontPath, patternsPath});
		//sposto i file di output
		Path datatypeAkpGrezzo_Updated = Paths.get("datatype-akp_grezzo_Updated.txt");
		Path objectAkpGrezzo_Updated = Paths.get("object-akp_grezzo_Updated.txt");
		Files.move(datatypeAkpGrezzo_Updated, datatypeAkpGrezzo_target, REPLACE_EXISTING);
		Files.move(objectAkpGrezzo_Updated, objectAkpGrezzo_target, REPLACE_EXISTING);
		Path datatypeAkp = Paths.get(patternsPath + "datatype-akp.txt");
		Path objectAkp = Paths.get(patternsPath + "object-akp.txt");
		Path datatypeAkp_Updated = Paths.get("datatype-akp_Updated.txt");
		Path objectAkp_Updated = Paths.get("object-akp_Updated.txt");
		Files.move(datatypeAkp_Updated, datatypeAkp, REPLACE_EXISTING);
		Files.move(objectAkp_Updated, objectAkp, REPLACE_EXISTING);
		Path countDatatypeProperties_Updated = Paths.get("count-datatype-properties_Updated.txt");
		Path countObjectProperties_Updated = Paths.get("count-object-properties_Updated.txt");
		Path countDatatypeProperties = Paths.get(patternsPath + "count-datatype-properties.txt");
		Path countObjectProperties = Paths.get(patternsPath + "count-object-properties.txt");
		Files.move(countDatatypeProperties_Updated, countDatatypeProperties, REPLACE_EXISTING);
		Files.move(countObjectProperties_Updated, countObjectProperties, REPLACE_EXISTING);

	}


	private void inference(String patternsPath, String ontPath) throws Exception{
		System.out.println("\"------------ PATTERN INFERENCE --------------------");
		checkFile(patternsPath + "AKPs_Grezzo-parts");
		checkFile(patternsPath+"specialParts_outputs");
		DatatypeSplittedPatternInference.main(new String[] {patternsPath, patternsPath + "AKPs_Grezzo-parts", ontPath, patternsPath+"specialParts_outputs"});
		ObjectSplittedPatternInference.main(new String[] {patternsPath, patternsPath + "AKPs_Grezzo-parts", ontPath, patternsPath+"specialParts_outputs"});
	}

	private void cardinality(String patternsPath, boolean richCardinalities) throws Exception {
		if(richCardinalities)
			System.out.println("\"------------ CARDINALITY CALCULATION (RICH) --------------------");
		else
			System.out.println("\"------------ CARDINALITY CALCULATION --------------------");
		checkFile(patternsPath + "/Akps");
		checkFile(patternsPath + "/Properties");
		MainCardinality.main(new String[] { patternsPath, String.valueOf(richCardinalities) });
	}

	private void checkFile(String path_dir) throws Exception {
		File dir = new File(path_dir);

		if (dir.exists())
			FileUtils.deleteDirectory(dir);
		if (dir.mkdirs())
			System.out.println("Successfully created:" + dir);
		else
			System.out.println("Failed to create: " + dir);
	}
}
