package com.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("spark")
@Service
public class HDFSReader implements FileReader{

	
	@Value("${hdfs.address}")
	String address;
	@Value("${hdfs.port}")
	String port;
	@Autowired
	DistributedFileSystem dfs;
	
	FileSystem fs;
	List<String> files;
	BufferedReader br;
	String currentLine;
	
	
	@Override
	public String[] nextLine() {
		return currentLine.split(";");
	}

	
	@Override
	public boolean hasNextLine() throws Exception {
		String line = br.readLine();
		if (line != null) {
			currentLine = line;
			return true;
		} 
		else {
			files.remove(0);
			while(!files.isEmpty()) {
				Path path = new Path(files.get(0));
				br = new BufferedReader(new InputStreamReader(fs.open(path)));
				line = br.readLine();
				if (line != null) {
					currentLine = line;
					return true;
				}
				else 
					files.remove(0);
			}
		}
		return false;	
	}

	
	@Override
	public void open(String dir) throws Exception {
		// getting file list contained in dir
		List<String> set = new ArrayList<String>();
		RemoteIterator<LocatedFileStatus> it = dfs.listFiles(new Path(dir), false);
		while(it.hasNext()){
	        LocatedFileStatus fileStatus = it.next();
	        set.add(fileStatus.getPath().toString());
	    }
		this.files = set;
		
		// config
		Configuration configuration = new Configuration();
		fs = FileSystem.get(new URI(address + ":" + port), configuration);
		
		// setting reader
		String pathString = files.get(0);
		Path path = new Path(pathString);
		br = new BufferedReader(new InputStreamReader(fs.open(path)));
	}
	
}
