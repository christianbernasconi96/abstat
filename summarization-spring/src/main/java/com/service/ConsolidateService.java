package com.service;

import com.model.SubmitConfig;

public interface ConsolidateService {


	public void init(String dataset, String domain, String id_current_summary, String countConcepts, String countDatatype, String countObjectProperties, String countDatatypeProperties);
	
	public void loadResources(String path, SubmitConfig config, String type) throws Exception ;
	
	public void loadAKPs(String path,SubmitConfig config, String type) throws Exception ;
	
	public void loadNumberOfInstances(String path,SubmitConfig config, String type) throws Exception ;
	
	public void loadPatternCardinalities(String path,SubmitConfig config) throws Exception ;
	
	public void loadPredictedCardinalities(String path,SubmitConfig config) throws Exception ;
	
	public void indexSummary(String path, String type) throws Exception;
	
	public void indexAKPsAutocomplete(String path, String type) throws Exception;
}
