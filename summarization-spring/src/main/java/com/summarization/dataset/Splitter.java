package com.summarization.dataset;

import java.io.File;
import java.io.FileOutputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Splitter{

	File destinationDir;
	public static final List<String> chars = Arrays.asList(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8",
			"9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
			"u", "v", "w", "x", "y", "z", "%", "_", "others" });
	Map<String, ArrayList<String>> outputs;         //maps each file path with a triples buffer to be written  
	ArrayList<String> error;
	public final int  bufferSize = 100000;
	
	
	public Splitter(File destinationDir) {
		try {
			this.destinationDir = destinationDir;
			outputs = new HashMap<String, ArrayList<String>>();
			error = new ArrayList<String>();
			for (String c : chars) {
				outputs.put(c + "_types.nt", new ArrayList<String>());
				outputs.put(c + "_dt_properties.nt", new ArrayList<String>());
				outputs.put(c + "_obj_properties.nt", new ArrayList<String>());
				new File(destinationDir + "/" + c + "_types.nt").createNewFile();
				new File(destinationDir + "/" + c + "_dt_properties.nt").createNewFile();
				new File(destinationDir + "/" + c + "_obj_properties.nt").createNewFile();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void split(InputFile file) throws Exception {
		Instant now = Instant.now();
		System.out.println("START " + file.name());
		String line;
		long count = 0;
		while (file.hasNextLine() && (line = file.nextLine()) != null && !line.equals("")) {
			count++;
			
			try {
				String[] el = func(line);
				String res = el[1].substring(el[1].lastIndexOf("/") + 1);

				String prefix;
				if (!res.equals("") && chars.contains(String.valueOf(res.toLowerCase().charAt(0))))
					prefix = String.valueOf(res.toLowerCase().charAt(0));
				else
					prefix = "others";

				if (el[0].equals("dt_typo")) {
					String riga = el[1] + "##" + el[2] + "##\"" + el[3] + "\"##" + el[4];               
					outputs.get(prefix + "_dt_properties.nt").add(riga);
				} else if (el[0].equals("dt_lang")) {
					String riga = el[1] + "##" + el[2] + "##\"" + el[3] + el[4] + "\"";
					outputs.get(prefix + "_dt_properties.nt").add(riga);
				} else if (el[0].equals("dt_simple")) {
					String riga = el[1] + "##" + el[2] + "##\"" + el[3] + ".\"";
					outputs.get(prefix + "_dt_properties.nt").add(riga);
				} else if (el[0].equals("typing")) {
					String riga = el[1] + "##" + el[2] + "##" + el[3];
					outputs.get(prefix + "_types.nt").add(riga);
				} else if (el[0].equals("obj")) {
					String riga = el[1] + "##" + el[2] + "##" + el[3];
					outputs.get(prefix + "_obj_properties.nt").add(riga);
				} else {
					error.add(el[1] + "\n");
				}
			
				if(count > this.bufferSize) {
					flushOutputs();
					System.out.println(this.bufferSize);
					count = 0;
				}
				//endProcessing();
			}
			catch(Exception e) {
				System.out.println("error processing triple:" + line);
			}
		}
		System.out.println(count);
		endProcessing();
		System.out.println(file.name() + ": " + Duration.between(now, Instant.now()).getSeconds() + "s");
	}

	
	public void flushOutputs() throws Exception {
		for(String key : this.outputs.keySet()) {
			ArrayList<String> triplesBuffer = this.outputs.get(key) ;
			if(!triplesBuffer.isEmpty()) {
				FileOutputStream fos = new FileOutputStream(destinationDir.getPath() + "/" + key, true);
				Iterator<String> it = triplesBuffer.iterator();
				while(it.hasNext()) {
					fos.write((it.next() + "\n").getBytes());
					it.remove();
				}
				fos.close();
			
			//	this.outputs.put(key, new ArrayList<String>());
			}
		}
		FileOutputStream fos = new FileOutputStream(destinationDir.getPath() + "/error.txt", true);
		for(String triple : error) 
			fos.write((triple + "\n").getBytes());
		
		fos.close();
		error =  new ArrayList<String>();
	}
	
	
	public String[] func(String line) {
		String s, p, o, dt;

		String[] splitted = line.split("> <http");
		s = splitted[0].substring(1);
		if (s.contains("<") || s.contains(">") || s.contains("\""))
			return new String[] { "error", line };

		if (splitted.length == 2) {                           //probably a datatype relational assertion
			String[] splitted2 = splitted[1].split("> \"");

			if (splitted2.length == 2) {
				p = "http" + splitted2[0];
				if (p.contains("<") || p.contains(">") || p.contains("\""))
					return new String[] { "error", line };

				String[] splitted3 = splitted2[1].split("\"\\^\\^");

				if (splitted3.length == 2) {
					o = splitted3[0];
					o = o.replace("##", "%23%23").replace("\\", "").replace("\"", "\\\"");   //to avoid \n \r, ecc to avoid problems caused by ## and "" inside ""
					dt = splitted3[1].substring(1, splitted3[1].length() - 3);
					return new String[] { "dt_typo", s, p, o, dt };
				} else {
					splitted3 = splitted2[1].split("\"@");
					if (splitted3.length == 2) {
						o = splitted3[0];
						o = o.replace("##", "%23%23").replace("\\", "").replace("\"", "\\\""); //to avoid \n \r, ecc to avoid problems caused by ## and "" inside ""
						dt = "@" + splitted3[1].substring(0, splitted3[1].length() - 2);
						return new String[] { "dt_lang", s, p, o, dt };
					} else {
						o = splitted3[0].substring(0, splitted3[0].length() - 2);
						o = o.replace("##", "%23%23").replace("\\", "").replace("\"", "\\\""); //to avoid \n \r, ecc to avoid problems caused by ## and "" inside ""
						return new String[] { "dt_simple", s, p, o };
					}
				}
			}

			if (splitted[1].split("> <").length >= 2) {     // for objects like <ftp://ftp.microsoft.com/deskapps/kids/3dmm.exe> .
				splitted2 = splitted[1].split("> <");
				o = splitted2[1].substring(0, splitted2[1].length() - 3);
				p = "http" + splitted2[0];
				if (p.contains("<") || p.contains(">") || p.contains("\""))
					return new String[] { "error", line };
				if (o.contains("<") || o.contains(">") || o.contains("\""))
					return new String[] { "error", line };
				o = o.replace("##", "%23%23");
				return new String[] { "obj", s, p, o };
			}
		}

		else if (splitted.length == 3) {               //type assertion or obj relational assertion
			p = "http" + splitted[1];
			o = "http" + splitted[2].substring(0, splitted[2].length() - 3);
			if (p.contains("<") || p.contains(">") || p.contains("\""))
				return new String[] { "error", line };
			if (o.contains("<") || o.contains(">") || o.contains("\""))
				return new String[] { "error", line };
			o = o.replace("##", "%23%23");

			if (p.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))
				return new String[] { "typing", s, p, o };
			else
				return new String[] { "obj", s, p, o };
		}

		return new String[] { "error", line };
	}
	
	

	public void endProcessing() throws Exception {
		flushOutputs();
	}
}
