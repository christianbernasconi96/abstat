package com.summarization.dataset;

import java.io.File;

public interface InputFile {

	public String absoluteName();
	
	public String name();
	
	public File parent();

	public String nextLine() throws Exception;

	public boolean hasNextLine();

	public InputFile reopen() throws Exception;
}