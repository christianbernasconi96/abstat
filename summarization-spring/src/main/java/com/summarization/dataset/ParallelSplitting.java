package com.summarization.dataset;

import java.io.File;

public class ParallelSplitting  implements Processing{
	File destinationDir;
	
	public ParallelSplitting(File destinationDir) {
		this.destinationDir = destinationDir;
	}
	
	@Override
	public void process(InputFile file) throws Exception {
		new Splitter(destinationDir).split(file);
	}

	@Override
	public void endProcessing() throws Exception {
	}

}
