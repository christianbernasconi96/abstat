package com.start.abstat;


import java.net.URI;

import javax.servlet.MultipartConfigElement;

import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableAsync
@Configuration
@ComponentScan({"com.controller", "com.model", "com.service", "com.dao", "com.frontend"})
@EnableSolrRepositories(basePackages="com.repository",multicoreSupport = true)
@EnableMongoRepositories(basePackages="com.repository")
public class MvcConfiguration extends WebMvcConfigurerAdapter {
	
	@Value("${hdfs.user}")
	private String user;

	@Value("${hdfs.address}")
	private String address;
	
	@Value("${hdfs.port}")
	private String port;

	
	@Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/view/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }

	
    @Override
    public void configureDefaultServletHandling(
            DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
    

	@Bean
	MultipartConfigElement multipartConfigElement() {
	    MultipartConfigFactory factory = new MultipartConfigFactory();
	    factory.setMaxFileSize("100000MB");
	    factory.setMaxRequestSize("100000MB");
	    return factory.createMultipartConfig();
	}
    
    
    @Bean(name="processExecutor")
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.setThreadNamePrefix("abstatThread");
        executor.initialize();
        return executor;

    }
    
   
	@Bean
	@Profile("spark")
	public DistributedFileSystem dfsBean() throws Exception{
		System.setProperty("HADOOP_USER_NAME", user);
		DistributedFileSystem dfs = new DistributedFileSystem();
		dfs.initialize(new URI(address + ":" + port), new org.apache.hadoop.conf.Configuration());
		return dfs;
	}
}
