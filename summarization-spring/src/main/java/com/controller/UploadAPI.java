package com.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.model.Dataset;
import com.model.Ontology;
import com.service.DatasetService;
import com.service.OntologyService;

import org.apache.hadoop.hdfs.DistributedFileSystem;
import  org.apache.hadoop.fs.Path;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping(value = "upload")
public class UploadAPI {
	
	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;
	@Value("${spring.profiles.active}")
	String profile;
	@Autowired(required = false)
	DistributedFileSystem dfs;
	@Value("${hdfs.user}")
	String user;
	@Value("${hdfs.address}")
	String address;
	@Value("${hdfs.port}")
	String port;
	@Value("${hdfs.abstatdir}")
	String abstatHDFSdir;
	
	private final String FS_FOLDER = "../data/DsAndOnt/";
	

	@RequestMapping(value = "/ds", method = RequestMethod.POST)
	public String datasetUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws Exception {
		if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:status";
        }
		try {
            InputStream stream = file.getInputStream();
                   
            Dataset dataset = new Dataset();
            dataset.setType("dataset");
            dataset.setName(file.getOriginalFilename().replaceFirst("[.][^.]+$", ""));
            SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
            dataset.setTimestamp( ft.format(new Date()));
            String dir_pat = FS_FOLDER + "dataset" + "/" + dataset.getName();
            dataset.setPath(dir_pat +"/"+ file.getOriginalFilename());

			File dir = new File(dir_pat);
			if (!dir.exists()) {
				if (dir.mkdirs()) System.out.println("Directory is created!");
				else System.out.println("Failed to create directory!");
			}

			FileUtils.copyInputStreamToFile(stream, new File(dataset.getPath()));
			if (profile.equals("spark")) {
				String HDFSPath = putInHDFS(new Path(dataset.getPath()), "dataset");
				dataset.setPath(HDFSPath);
			}
			datasetService.add(dataset);
			redirectAttributes.addFlashAttribute("message", "You successfully uploaded '" + file.getName() + "'");
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:status";
    }
	
	
	@RequestMapping(value = "/ont", method = RequestMethod.POST)
	public String ontologyUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes)  throws Exception{
		if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:status";
        }
		try {
            InputStream stream = file.getInputStream();
            
            Ontology ontology = new Ontology();
            ontology.setType("ontology");
            ontology.setName(file.getOriginalFilename().replaceFirst("[.][^.]+$", ""));
	        SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
            ontology.setTimestamp( ft.format(new Date()));
            String dir_path = FS_FOLDER + "ontology";
            ontology.setPath(dir_path  +"/"+ file.getOriginalFilename());
            
			File dir = new File(dir_path);
			if (!dir.exists()) {
				if (dir.mkdirs()) System.out.println("Directory is created!");
				else System.out.println("Failed to create directory!");
			}
			
			FileUtils.copyInputStreamToFile(stream, new File(ontology.getPath()));
			if (profile.equals("spark")) {
				String HDFSPath = putInHDFS(new Path(ontology.getPath()), "ontology");
				ontology.setPath(HDFSPath);
			}
        	ontologyService.add(ontology);
        	redirectAttributes.addFlashAttribute("message", "You successfully uploaded '" + file.getOriginalFilename() + "'");
        	
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:status";
    }
	
	
	public String putInHDFS(Path src, String type) throws Exception {
		System.out.println("Copying into HDFS...");
		Path destDir;
		if(type.equals("dataset"))
			destDir = new Path(address + ":" + port + abstatHDFSdir + "dataset/" );
		else
			destDir = new Path(address + ":" + port + abstatHDFSdir +"ontology/" );
		
		if (!dfs.exists(destDir)) {
			dfs.mkdirs(destDir);
			System.out.println("created directory");
		}
		dfs.copyFromLocalFile(src, destDir);
		System.out.println("File written on HDFS successfully!");
		String fileLocation = destDir.toString()  + src.toString().substring(src.toString().lastIndexOf('/'));
		return fileLocation;
	}
	
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
    public String uploadStatus() {
        return "operationStatus";
    }
	
	
}
