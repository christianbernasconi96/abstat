package com.controller;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.SubmitConfig;
import com.service.ConsolidateService;
import com.service.SubmitConfigService;

@CrossOrigin(origins = "*")
@Controller
public class ConsolidateAPI {
	
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	ConsolidateService consolidateService;
	

	@RequestMapping(value = "/consolidate", method = RequestMethod.POST)
	public String consolidate(@RequestParam(value="summary", required=true) String summary, 
			@RequestParam(value="store", required=false) Boolean store,
			@RequestParam(value="index", required=false) Boolean index,
			@RequestParam(value="domain", required=false) String domain) throws Exception{
		
		if(store==null) store=false;
		if(index==null) index=false;
	
		SubmitConfig config = submitConfigService.findSubmitConfigById(summary);
		String id_current_summary = config.getId();
		String patternsPath = config.getSummaryPath() + "/patterns/";
		String dataset = config.getDsName();
		
		String countConcepts = patternsPath + "/count-concepts.txt";
		String countDatatype = patternsPath + "/count-datatype.txt";
		String countObjectProperties = patternsPath + "/count-object-properties.txt";
		String countDatatypeProperties = patternsPath + "/count-datatype-properties.txt";
		String objectAKP = patternsPath + "/object-akp.txt";
		String datatypeAKP = patternsPath + "/datatype-akp.txt";
		String objectPatterns = patternsPath + "/patterns_splitMode_object.txt";
		String datatypePatterns = patternsPath + "/patterns_splitMode_datatype.txt";
		String patternCardinalities = patternsPath + "/patternCardinalities.txt";
		String predictedCardinalities = patternsPath + "/predictedCardinalities.txt";
		
		consolidateService.init(dataset, domain, id_current_summary, countConcepts, countDatatype, countObjectProperties, countDatatypeProperties);
		
		if(store & !config.isLoadedMongoDB()) {
			consolidateService.loadResources(countConcepts, config, "Concept");
			consolidateService.loadResources(countDatatype, config,  "Datatype");
			consolidateService.loadResources(countObjectProperties, config, "Object Property");
			consolidateService.loadResources(countDatatypeProperties, config, "Datatype Property");
			consolidateService.loadAKPs(objectAKP, config, "Object AKP");
			consolidateService.loadAKPs(datatypeAKP, config, "Datatype AKP");
			
			if(new File(objectPatterns).exists() && new File(datatypePatterns).exists()) {
				consolidateService.loadNumberOfInstances(objectPatterns, config, "Object AKP");
				consolidateService.loadNumberOfInstances(datatypePatterns, config, "Datatype AKP");
			}
			
			if(new File(patternCardinalities).exists()) {
				consolidateService.loadPatternCardinalities(patternCardinalities, config);
			}
			
			if(new File(predictedCardinalities).exists()) {
				consolidateService.loadPredictedCardinalities(predictedCardinalities, config);
			}
		}
		
		if(index & !config.isIndexedSolr()) {
			consolidateService.indexSummary(countConcepts, "concept");
			consolidateService.indexSummary(countDatatype,  "datatype");
			consolidateService.indexSummary(countObjectProperties, "objectProperty");
			consolidateService.indexSummary(countDatatypeProperties, "datatypeProperty");
			consolidateService.indexSummary(objectAKP, "objectAkp");
			consolidateService.indexSummary(datatypeAKP, "datatypeAkp");
			consolidateService.indexAKPsAutocomplete(objectAKP, "objectAkp");
			consolidateService.indexAKPsAutocomplete(datatypeAKP, "datatypeAkp");	
		}
		
		// update summary config 
		if(index) config.setIndexedSolr(index);
		if(store) config.setLoadedMongoDB(store);
		submitConfigService.update(config);
		
		return "redirect:home";
	}
	
	
}
