package com.controller;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.model.SubmitConfig;
import com.repository.AKPRepo;
import com.repository.ResourceRepo;
import com.service.AKPService;
import com.service.ResourceService;
import com.service.SubmitConfigService;

@CrossOrigin(origins = "*")
@Controller
public class SummaryCRUDAPI {

	
	@Autowired
	SubmitConfigService submitConfigService;
	@Autowired
	AKPRepo akpRepo;
	@Autowired
	ResourceRepo resRepo;
	@Autowired 
	ResourceService resourceService;
	@Autowired 
	AKPService AKPservice;
	
	
	@RequestMapping(value = "/submitconfig/delete/{id}", method = RequestMethod.POST)
		public String delete(@PathVariable("id") String id) throws IOException {
			SubmitConfig submitConfig = submitConfigService.findSubmitConfigById(id);
			
			akpRepo.deleteBySummary(submitConfig.getId());
			resRepo.deleteBySummary(submitConfig.getId());
			
			AKPservice.deletebySummary(submitConfig.getId());
			resourceService.deletebySummary(submitConfig.getId());
			
			submitConfigService.delete(submitConfig);
			
			return "redirect:/management";
	}
	
	
	@RequestMapping(value = "/submitconfig/deleteDir/{id}", method = RequestMethod.POST)
	public String deleteDir(@PathVariable("id") String id) throws IOException {
		SubmitConfig submitConfig = submitConfigService.findSubmitConfigById(id);
		
		akpRepo.deleteBySummary(submitConfig.getId());
		resRepo.deleteBySummary(submitConfig.getId());
		
		AKPservice.deletebySummary(submitConfig.getId());
		resourceService.deletebySummary(submitConfig.getId());
		
		submitConfigService.delete(submitConfig);
		FileUtils.deleteDirectory(new File(StringUtils.substringBeforeLast(submitConfig.getSummaryPath(), "/")));
		
		return "redirect:/management";
	}
	
	
	@RequestMapping(value="/api/v1/summaries", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getitem( @RequestParam(value="loaded", required=false) Boolean loaded,
										 @RequestParam(value="indexed", required=false) Boolean indexed,
										 @RequestParam(value="search", required=false) String search) {
		String summaries = submitConfigService.listSubmitConfigJSON(loaded, indexed, search);
		return summaries;		
	}
}
