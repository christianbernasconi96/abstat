package com.controller;

import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@CrossOrigin(origins = "*")
@Controller
public class SearchAPI {


	@RequestMapping(value="/api/v1/search", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String search(
			@RequestParam(value="q", required=true) String q,
			@RequestParam(value="dataset", required=false) String dataset,
			@RequestParam(value="rows", required=true) Integer rows,
			@RequestParam(value="start", required=true) Integer start,
			@RequestParam(value="includeNPref", required=false) Boolean includeNPref)  throws Exception{
		String urlString = "http://localhost:8983/solr/indexing/select?q=fullTextSearchField:(" + q +")&start=" + start + "&rows=" + rows + "&wt=json";
		if(dataset!=null)
			urlString += "&fq=dataset:" + dataset;
		if( includeNPref==null || !includeNPref)
			urlString += "&fq=subtype:+internal";
	
	    JSONObject out = new JSONObject();
	    JSONArray results = new JSONArray();
		
		InputStream solrOutput = new URL(urlString).openStream();
		Object obj = new JSONParser().parse(IOUtils.toString(solrOutput, StandardCharsets.UTF_8));
		JSONObject json = (JSONObject) obj;
	    JSONObject data = (JSONObject) json.get("response");
	    JSONArray docs = (JSONArray) data.get("docs");
	
	    for(int i=0; i<docs.size(); i++){
	    	JSONObject doc = (JSONObject) docs.get(i);
    		JSONArray URI = (JSONArray)doc.get("URI");	
    		
    		JSONObject result = new JSONObject();
    		result.put("subject", URI.get(0));
    		if(URI.size()>1) {
	    		result.put("predicate", URI.get(1));
	    		result.put("object", URI.get(2));
    		}
    		result.put("frequency", doc.get("occurrence"));
    		result.put("type", doc.get("type"));
    		result.put("subtype", doc.get("subtype"));
    		result.put("dataset", doc.get("dataset"));
    		result.put("summary", doc.get("summary"));
    		
    		results.add(result);
	    }
	    
    	out.put("results", results);
		return out.toJSONString();
	}
	
}
