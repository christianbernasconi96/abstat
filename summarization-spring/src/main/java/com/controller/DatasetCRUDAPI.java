package com.controller;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.model.Dataset;
import com.service.DatasetService;

@CrossOrigin(origins = "*")
@Controller
public class DatasetCRUDAPI {

	@Autowired
	DatasetService datasetService;

	@RequestMapping(value = "/dataset/delete/{id}", method = RequestMethod.POST)
		public String delete(@PathVariable("id") String id) throws IOException {
			Dataset dataset = datasetService.findDatasetById(id);
			datasetService.delete(dataset);
			return "redirect:/management";
	}
	

	@RequestMapping(value = "/dataset/deleteDir/{id}", method = RequestMethod.POST)
	public String deleteDir(@PathVariable("id") String id) throws IOException {
		Dataset dataset = datasetService.findDatasetById(id);
		datasetService.delete(dataset);
		FileUtils.deleteDirectory(new File(StringUtils.substringBeforeLast(dataset.getPath(), "/")));
		return "redirect:/management";
	}
	
	
	@RequestMapping(value="/api/v1/datasets", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getitem() {
		String datasets = datasetService.listDatasetJSON();
		return datasets;
	}
}
