package com.test.system;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;
import org.junit.Assert;
import org.junit.Before; 

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class SummarizeAPITest {

	  @Autowired
	  private MockMvc mockMvc;

	  @Before
	  public void shouldSummarize() throws Exception {
		  this.mockMvc.perform(post("/summarizator")
	        		.param("dataset", "system-test_dataset")
	        		.param("ontologies", "system-test_ontology")
	        		.param("concept_min", "true")
	        		.param("inference", "true")
	        		.param("cardinality", "true")
	        		.param("property_min", "true")
	        		.param("rich_cardinalities", "true")
	        		.param("shacl_validation", "true")
	        		.param("async", "false"))
	        .andDo(print()).andExpect(status().is2xxSuccessful());
	  }
	  
	  
	  @Test
	  public void shouldContainsSummaryFiles() {
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/count-concepts.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/count-datatype.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/count-datatype-properties.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/count-object-properties.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/datatype-akp.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/datatype-akp_grezzo.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/object-akp.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/object-akp_grezzo.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/patterns_splitMode_datatype.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/patterns_splitMode_object.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/patternCardinalities.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/predictedCardinalities.txt").exists());
			Assert.assertTrue(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl/patterns/shacl/shapes/").exists());
	  }
	  
	  
}
