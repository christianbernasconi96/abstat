package com.test.system;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.model.SubmitConfig;
import com.service.SubmitConfigService;
import com.start.abstat.AbstatApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class ConsolidateAPITest {

	  @Autowired
	  private MockMvc mockMvc;
	  @Autowired
	  private SubmitConfigService service;
	  
	  private String idSummary;
	  
	  @Before
	  public void init() {
		  List<SubmitConfig> list = service.listSubmitConfig();
		  for(SubmitConfig config : list)
			  if(config.getDsId().equals("system-test_dataset") && 
					  	config.getListOntId().get(0).equals("system-test_ontology") && 
					  	config.isTipoMinimo() && 
					  	config.isInferences() && 
					  	config.isCardinalita() && 
					  	config.isPropertyMinimaliz() &&
					  	config.isShaclValidation()) 
				  idSummary=config.getId();
	  }
	  
	  @Test
	  public void shouldWork() throws Exception {
		  this.mockMvc.perform(post("/consolidate")
	        		.param("summary", idSummary)
	        		.param("store", "true")
		  			.param("index", "true")
		  			.param("domain", "dbpedia.org"))
	        .andDo(print()).andExpect(status().is3xxRedirection());
	  }
}
