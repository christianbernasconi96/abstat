package com.test.system;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class AutocompleteAPINoConstraintTest {

    @Autowired
    private MockMvc mockMvc;

    
    @Test
    public void shouldBeReachable() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions?qString=ak&qPosition=pred")).andDo(print())
        .andExpect(status().isOk());
    }

    
    @Test
    public void shouldReturnFields() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"prefix\":", 4)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\":", 4)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"namespace\":", 4)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"occurrence\":", 4)))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"dataset\":", 4)));
    }
    
    @Test
    public void shortQStringShouldReturnEmptyListIfNotMaches() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "xj")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    @Test
    public void qStringShouldReturnEmptyListIfNotMaches() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "xjkl")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test"))
                .andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    
    
 /*################################################ MONO-DATASET ################################################*/
  
   //-------------------------------------- CHECK QSTRING & QPOSITION --------------------------------------------*/
    
    @Test
    public void shortqStringShouldWorkOnSubj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("a")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 4)));
    }
    @Test
    public void shortqStringShouldWorkOnSubj2() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "ag")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("ag")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    @Test
    public void qStringShouldWorkOnSubj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "person")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.containsQString("person")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 4)));
    }
    
 //------
    
    @Test
    public void shortqStringShouldWorkOnPred() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "m")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("m")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    @Test
    public void shortqStringShouldWorkOnPred2() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "mi")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("mi")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    @Test
    public void qStringShouldWorkOnPred() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "stat")
        		.param("qPosition", "subj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.containsQString("stat")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 1)));
    }
    
 //------
    
    @Test
    public void shortqStringShouldWorkOnObj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "p")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("p")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 6)));
    }
    
    @Test
    public void shortqStringShouldWorkOnObj2() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "pl")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.startWirhQString("pl")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    @Test
    public void qStringShouldWorkOnObj() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "kilo")
        		.param("qPosition", "obj")
        		.param("dataset", "system-test"))
                .andExpect(content().string(SuggestionMatcher.containsQString("kilo")))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    
    
    //------------------------------------------ CHECK PAGING ------------------------------------------------------*/
    
    
    @Test
    public void shouldWorkWithValidLRows() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("rows", "2"))
                .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 2)));
    }
    @Test
    public void shouldWorkWithInvalidRows() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("rows", "-4"))
        		.andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    
 //-----
    
    @Test
    public void shouldWorkWithValidLStart() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("start", "1"))
        .andExpect(content().string(not(containsString("\"suggestion\":\"http:\\/\\/dbpedia.org\\/ontology\\/PopulatedPlace\\/areaTotal\""))));
    }
    @Test
    public void shouldWorkWithInvalidStart() throws Exception {
        this.mockMvc.perform(get("/api/v1/SolrSuggestions")
        		.param("qString", "a")
        		.param("qPosition", "pred")
        		.param("dataset", "system-test")
        		.param("start", "100"))
        		.andExpect(content().string(containsString("{\"suggestions\":[]}")));
    }
    
 //-----
    
    @Test
    public void pagingSholdWork() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "a")
         		.param("qPosition", "pred")
        		.param("dataset", "system-test")
    			.param("start", "0")
    			.param("rows", "3"))
    	 .andExpect(content().string(containsString("http:\\/\\/dbpedia.org\\/ontology\\/PopulatedPlace\\/areaTotal")))
    	 .andExpect(content().string(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaTotal")))
    	 .andExpect(content().string(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaLand")))
    	 .andExpect(content().string(not(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaWater"))));
    }
    @Test
    public void pagingSholdWork2() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "a")
         		.param("qPosition", "pred")
        		.param("dataset", "system-test")
    			.param("start", "1")
    			.param("rows", "2"))
    	 .andExpect(content().string(not(containsString("http:\\/\\/dbpedia.org\\/ontology\\/PopulatedPlace\\/areaTotal"))))
    	 .andExpect(content().string(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaTotal")))
    	 .andExpect(content().string(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaLand")))
    	 .andExpect(content().string(not(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaWater"))));
    }
    @Test
    public void pagingSholdWork3() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "a")
         		.param("qPosition", "pred")
        		.param("dataset", "system-test")
    			.param("start", "2")
    			.param("rows", "3"))
    	 .andExpect(content().string(not(containsString("http:\\/\\/dbpedia.org\\/ontology\\/PopulatedPlace\\/areaTotal"))))
    	 .andExpect(content().string(not(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaTotal"))))
    	 .andExpect(content().string(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaLand")))
    	 .andExpect(content().string(containsString("http:\\/\\/dbpedia.org\\/ontology\\/areaWater")));
    }
    @Test
    public void pagingSholdWork4() throws Exception{
    	 mockMvc.perform(get("/api/v1/SolrSuggestions")
         		.param("qString", "a")
         		.param("qPosition", "pred")
        		.param("dataset", "system-test")
    			.param("start", "0")
    			.param("rows", "100"))
    	 .andExpect(content().string(StringMatcher.containsStringNTimes("\"suggestion\"", 4)));
    }

    
    //------------------------------------------ CHECK RANKING ------------------------------------------------------*/
    
    @Test
    public void shouldBeRankDescOnFrequency() throws Exception{
		ArrayList<String> list = new ArrayList<String>();
		list.add("\"occurrence\":4");
		list.add("\"occurrence\":3");
		list.add("\"occurrence\":3");
		list.add("\"occurrence\":1");
		list.add("\"occurrence\":1");
		list.add("\"occurrence\":1");

    	mockMvc.perform(get("/api/v1/SolrSuggestions")
    			.param("qString", "p")
         		.param("qPosition", "subj")
        		.param("dataset", "system-test"))
    	 .andExpect(content().string(stringContainsInOrder(list)));
    }

    
    // TODO
    /*####################################################### MULTI-DATASET ###########################################################*/
}
