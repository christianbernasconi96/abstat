package com.test.system;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.start.abstat.AbstatApplication;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class TriplesExtractorAPISingleTest {
	@Autowired
	private MockMvc mockMvc;

	private String dataset;
	private String ontology;
	private String dboMusicalArtist;
	private String owlThing;
	private String xsdInteger;
	private String rdfsLiteral;
	private String dboChild;
	private String dboAge;
	private String foafName;
	private String concept;
	private String datatype;
	private String maxDirect;
	private String maxInverse;
	// istances
	private String typedSubject;
	private String untypedSubject;
	private String typedObject;
	private String untypedObject;
	private String datatypeObject;
	private String literalObjectTypedSubject;
	private String literalObjectUntypedSubject;


	@Before
	public void init() {
		dataset = "extraction-test_dataset";
		ontology = "extraction-test_ontology";
		dboMusicalArtist = "http://dbpedia.org/ontology/MusicalArtist";
		owlThing = "http://www.w3.org/2002/07/owl#Thing";
		xsdInteger = "http://www.w3.org/2001/XMLSchema#integer";
		rdfsLiteral = "http://www.w3.org/2000/01/rdf-schema#Literal";
		dboChild = "http://dbpedia.org/ontology/child";
		dboAge = "http://dbpedia.org/ontology/age";
		foafName = "http://xmlns.com/foaf/0.1/name";
		concept = "Concept AKP";
		datatype = "Datatype AKP";
		maxDirect = "MaxSubjObjs";
		maxInverse = "MaxSubjsObj";
		// istances
		typedSubject = "http://dbpedia.org/resource/MusicalArtist_KO_1";
		untypedSubject = "http://dbpedia.org/resource/UndefinedSubj_KO_1";
		typedObject = "http://dbpedia.org/resource/MusicalArtist_KO_12_Child_1";
		untypedObject = "http://dbpedia.org/resource/UndefinedSubj_KO_12_UndefinedChild_1";
		datatypeObject = "333";
		literalObjectTypedSubject = "MusicalArtist_KO_12 Name_1";
		literalObjectUntypedSubject = "UndefinedSubj_KO_12 Name_1";
	}

	/*------------------------------------------- output fields -----------------------------------------------*/

	@Test
	public void shouldShowFieldsMaxDirect() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", typedSubject)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"head\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"vars\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"results\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"bindings\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 12)));
	}

	@Test
	public void shouldShowFieldsMaxInverse() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", typedObject)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"head\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"vars\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"results\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"bindings\":", 1)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)));
	}

	/*--------------------------------- check results MaxSubjObjs -------------------------------------------*/
	// TODO: sistemare API per akpType

	@Test
	public void shouldExtractThingDirect() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", typedSubject)
				.param("pred", dboChild)
				.param("obj", owlThing)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 12)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + typedSubject + "\"", 4))) // subj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_UndefinedChild_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_UndefinedChild_2\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_1\"", 1))) // obj3
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_UndefinedChild_2\"", 1))) // obj4
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test 
	public void shouldExtractConceptDirect() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", typedSubject)
				.param("pred", dboChild)
				.param("obj", dboMusicalArtist)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxDirect)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 4)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 12)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + typedSubject + "\"", 4))) // subj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_Child_1\"", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1_Child_2\"", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_1\"", 1))) // obj3
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_12_Child_2\"", 1))) // obj4
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test
	public void shouldExtractDatatypeDirect() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", typedSubject)
				.param("pred", dboAge)
				.param("obj", xsdInteger)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + typedSubject + "\"", 4))) // subj
		.andExpect(content().string(StringMatcher.containsStringNTimes("21", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("22", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test
	public void shouldExtractLiteralDirect() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", typedSubject)
				.param("pred", foafName)
				.param("obj", rdfsLiteral)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxDirect)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + typedSubject + "\"", 4))) // subj
		.andExpect(content().string(StringMatcher.containsStringNTimes("21", 1))) // obj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("22", 1))) // obj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	/*--------------------------------- check results MaxSubjsObj -------------------------------------------*/
	// TODO
	@Test
	public void shouldExtractThingInverse() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", owlThing)
				.param("pred", dboChild)
				.param("obj", untypedObject)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + untypedObject + "\"", 2))) // obj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test
	public void shouldExtractConceptInverse() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboChild)
				.param("obj", typedObject)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", concept)
				.param("cardinalityType", maxInverse)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"" + typedObject + "\"", 2))) // obj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test
	public void shouldExtractThingDatatypeInverse() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", owlThing)
				.param("pred", dboAge)
				.param("obj", datatypeObject)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes(datatypeObject, 2))) // obj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test
	public void shouldExtractConceptDatatypeInverse() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", dboAge)
				.param("obj", datatypeObject)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes(datatypeObject, 2))) // obj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test
	public void shouldExtractThingLiteralInverse() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", owlThing)
				.param("pred", foafName)
				.param("obj", literalObjectUntypedSubject)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes(literalObjectUntypedSubject, 2))) // obj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/UndefinedSubj_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of typed violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	@Test
	public void shouldExtractConceptLiteralInverse() throws Exception{
		mockMvc.perform(get("/singleExtractor")
				.param("subj", dboMusicalArtist)
				.param("pred", foafName)
				.param("obj", literalObjectTypedSubject)
				.param("dataset", dataset)
				.param("ontology", ontology)
				.param("akpType", datatype)
				.param("cardinalityType", maxInverse)
				.param("limit", "100")
				.param("offset", "0"))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"subj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"pred\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"obj\":", 2)))
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"value\":", 6)))
		.andExpect(content().string(StringMatcher.containsStringNTimes(literalObjectTypedSubject, 2))) // obj
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_1\"", 1))) // subj1
		.andExpect(content().string(StringMatcher.containsStringNTimes("\"http://dbpedia.org/resource/MusicalArtist_KO_2\"", 1))) // subj2
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Artist", 0))) // check absence of supertype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("/Guitarist", 0))) // check absence of subtype violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("Undefined", 0))) // check absence of untyped violations
		.andExpect(content().string(StringMatcher.containsStringNTimes("_OK", 0))) // check absence of false violations
		;
	}

	
}
