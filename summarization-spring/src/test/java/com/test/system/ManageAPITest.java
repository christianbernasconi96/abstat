package com.test.system;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.model.SubmitConfig;
import com.repository.AKPRepo;
import com.repository.ResourceRepo;
import com.service.AKPService;
import com.service.ResourceService;
import com.service.SubmitConfigService;
import com.service.DatasetService;
import com.service.OntologyService;

import com.start.abstat.AbstatApplication;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = AbstatApplication.class)
@AutoConfigureMockMvc
public class ManageAPITest {
	
	@Autowired
    MockMvc mockMvc;
	@Autowired
	SubmitConfigService submitconfigService;
	@Autowired
	AKPService AkpService;
	@Autowired
	ResourceService resourceService;
	@Autowired
	DatasetService datasetService;
	@Autowired
	OntologyService ontologyService;
	@Autowired
	AKPRepo akpRepo;
	@Autowired
	ResourceRepo resRepo;
	
	String idSummary;
	static boolean flag = true;
	
	
	@Before
	public void shouldBeOK() throws Exception{
		//flag is used to avoid multiexecution of this code
		if(flag) {
			for(SubmitConfig config : submitconfigService.listSubmitConfig()) {
				if(config.getDsId().equals("system-test_dataset") && 
						config.getListOntId().get(0).equals("system-test_ontology") && 
						config.isTipoMinimo() && 
						config.isInferences() && 
						config.isCardinalita() && 
						config.isPropertyMinimaliz()) {
					idSummary=config.getId();
				}
			}
			
			flag = false;
			mockMvc.perform(post("/submitconfig/deleteDir/" + idSummary))
			.andDo(print()).andExpect(status().is3xxRedirection());

			mockMvc.perform(post("/dataset/delete/system-test_dataset"))
			.andDo(print()).andExpect(status().is3xxRedirection());
	
			mockMvc.perform(post("/ontology/delete/system-test_ontology"))
			.andDo(print()).andExpect(status().is3xxRedirection());
		}
	}
	
	
	@Test
	public void shouldNoContainSummaryFiles() {
		Assert.assertFalse(new File("../data/summaries/system-test_dbpedia_2014_MinTpPropMinCardInfShacl").exists());
	}
	
	
	@Test
	public void shouldNoContainSummaryOnMongo() {
		Assert.assertTrue(AkpService.getAKPs(idSummary).isEmpty());
		Assert.assertTrue(resourceService.getResources(idSummary).isEmpty());
	}

	
	@Test 
	public void shouldNoContainSummaryOnSolr() {
		Assert.assertTrue(akpRepo.findBySummary(idSummary).isEmpty());
		Assert.assertTrue(resRepo.findBySummary(idSummary).isEmpty());
	}
	
	
	@Test
	public void shouldNoContainDatasetMetadataOnMongo() {
		Assert.assertTrue(datasetService.findDatasetById("system-test_dataset") == null);
	}
	
	
	@Test
	public void shouldNoContainOntologyMetadataOnMongo() {
		Assert.assertTrue(ontologyService.findOntologyById("system-test_ontology") == null);
	}
	
}
