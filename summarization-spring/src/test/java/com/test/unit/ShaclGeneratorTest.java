package com.test.unit;
import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import com.summarization.export.CalculateCardinality;
import com.summarization.export.GenerateShacl;
import com.summarization.shacl.ShaclGenerator;




public class ShaclGeneratorTest extends TestWithTemporaryData {



	@Test
	public void testShapeContent() throws Exception{
		// TODO: test with definitive shape format
		// generating predictedCardinalities.txt
		String contentPredicted = "http://dbpedia.org/ontology/Person##http://dbpedia.org/ontology/birthDate##http://www.w3.org/2001/XMLSchema#date##2##3";
		temporary.namedFile(contentPredicted, "predictedCardinalities.txt");
		// generating count-datatype.txt
		String contentDatatype = "http://www.w3.org/2001/XMLSchema#date##1";
		temporary.namedFile(contentDatatype, "count-datatype.txt");
		// generating count-concepts.txt
		String contentConcepts = "http://dbpedia.org/ontology/Person##1";
		temporary.namedFile(contentConcepts, "count-concepts.txt");

		String shapesPath = temporary.path() + "/shacl/shapes/";
		GenerateShacl.main(new String [] {temporary.path(), shapesPath});
		// TODO: read shape from file
		String generatedShape = "shape";
		// TODO: construct expected shape
		String expectedShape = "shape";

		assertEquals(expectedShape, generatedShape);
	}

	@Test
	public void shouldGenerateNFiles() throws Exception{
		int n = 3;
		// generating predictedCardinalities.txt
		String content = "http://dbpedia.org/ontology/Person##http://dbpedia.org/ontology/birthDate##http://www.w3.org/2001/XMLSchema#date##2##3\n"
				+ "http://schema.org/Person##http://dbpedia.org/ontology/residence##http://schema.org/City##1##3\n"
				+ "http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#NaturalPerson##http://dbpedia.org/ontology/residence##http://dbpedia.org/ontology/Place##1##1";
		temporary.namedFile(content, "predictedCardinalities.txt");
		// generating count-datatype.txt
		String contentDatatype = "http://www.w3.org/2001/XMLSchema#date##1";
		temporary.namedFile(contentDatatype, "count-datatype.txt");
		// generating count-concepts.txt
		String contentConcepts = "http://dbpedia.org/ontology/Person##1\n"
				+ "http://schema.org/City##1\n"
				+ "http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#NaturalPerson##1\n"
				+ "http://dbpedia.org/ontology/Place##1";
		temporary.namedFile(contentConcepts, "count-concepts.txt");

		String shapesPath = temporary.path() + "/shacl/shapes/";
		GenerateShacl.main(new String [] {temporary.path(), shapesPath});

		assertEquals(n, new File(shapesPath).list().length);


	}

	@Test
	public void shouldGenerateFileWithName() throws Exception{
		// generating predictedCardinalities.txt
		String contentPredicted = "http://dbpedia.org/ontology/Person##http://dbpedia.org/ontology/birthDate##http://www.w3.org/2001/XMLSchema#date##2##3";
		temporary.namedFile(contentPredicted, "predictedCardinalities.txt");
		// generating count-datatype.txt
		String contentDatatype = "http://www.w3.org/2001/XMLSchema#date##1";
		temporary.namedFile(contentDatatype, "count-datatype.txt");
		// generating count-concepts.txt
		String contentConcepts = "http://dbpedia.org/ontology/Person##1";
		temporary.namedFile(contentConcepts, "count-concepts.txt");

		String shapesPath = temporary.path() + "/shacl/shapes/";
		GenerateShacl.main(new String [] {temporary.path(), shapesPath});
		String expected = "dbo#Person##dbo#birthDate##xmls#date.ttl";
		String filename = new File(shapesPath).list()[0];

		assertEquals(expected, filename);
	}



}
