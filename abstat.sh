#! /bin/bash

function start_enviroment(){
	echo "Starting MongoDB"
	service mongod start

	echo "Starting nginx"
	service nginx start
	service nginx stop
	service nginx start

	echo "Starting Solr"
	./deployment/solr.sh start

	echo "Starting Virtuoso"
	sudo service virtuoso-opensource-6.1 start
}

function start(){
	echo "Starting ABSTAT"
	cd summarization-spring
	java -Xmx8000m -Xms256m -jar target/summarization-spring-0.0.1-SNAPSHOT.jar
}


function build(){
	cd summarization-spring
	mvn package --quiet -Dmaven.test.skip=true -Dstart-class=com.start.abstat.AbstatApplication	
}


function install(){
	sudo apt-get update

	echo
	echo "Installing nginx -------------------------------------------------------------------------------------"
	sudo apt-get install -y nginx

	echo
	echo "Installing mongodb ----------------------------------------------------------------------------------------"
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
	sudo apt-get update
	ubuntu_version=$(lsb_release -a | grep "Release")

	if [[ $ubuntu_version == *"16"* ]]; then
		echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
	else
		echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
	fi

	sudo apt-get update
	sudo apt-get install -y mongodb-org

	echo
	echo "Installing java----------------------------------------------------------------------------------------"
	sudo apt-get install openjdk-8-jdk

	echo
	echo "Installing maven ----------------------------------------------------------------------------------------"
	sudo apt-get install -y maven

	echo
	echo "Installing gawk ----------------------------------------------------------------------------------------"
	sudo apt-get install -y gawk

	echo
	echo "Installing virtuoso ----------------------------------------------------------------------------------------"
	sudo apt-get install -y virtuoso-opensource


	if [[ $1 == -distributed ]]
	then
		echo
		echo "Installing abstat distributed -----------------------------------------------------------------------------------"
		git clone https://github.com/rAlvaPrincipe/abstat-distributed
		cd abstat-distributed/summarization-spark
		mvn package
	fi

	
}


function config(){
	relative_path=`dirname $0`
	root=`cd $relative_path;pwd`

	echo
	echo "Configuring Nginx -------------------------------------------------------------------------------------"
	baseLog_dir="$root/data/logs/reverse-proxy"
	accesLog_file="$baseLog_dir/access.log"
	errorLog_file="$baseLog_dir/error.log"
	nginx_port=$1
	application_port=$2
	#create log files 
	mkdir -p $baseLog_dir
	touch  $accesLog_file
	touch  $errorLog_file

	cd deployment
	#setting ports on config file	
	sed -i  "s/nginx_port/$nginx_port/g" nginx-model.conf
	sed -i  "s/application_port/$application_port/g" nginx-model.conf
	#setting paths on config file
	sed -i "s~root_path~$baseLog_dir~g" nginx-model.conf
	sed -i "s~access.log_path~$accesLog_file~g" nginx-model.conf
	sed -i "s~error.log_path~$errorLog_file~g" nginx-model.conf

	cp -rf nginx-model.conf /etc/nginx/nginx.conf

	#restoring previous labels to avoid git 
	sed -i "s~$baseLog_dir~root_path~g" nginx-model.conf
	sed -i "s~$accesLog_file~access.log_path~g" nginx-model.conf
	sed -i "s~$errorLog_file~error.log_path~g" nginx-model.conf
	sed -i "s~listen $nginx_port default_server~listen nginx_port default_server~g" nginx-model.conf
	sed -i "s~server 127.0.0.1:$application_port~server 127.0.0.1:application_port~g" nginx-model.conf

	cd "$root/summarization-spring/src/main/resources"
	sed -i  "s/^server.*$/server\.port = $application_port/g" application.properties

	cd $root

	echo "Configuring virtuoso -------------------------------------------------------------------------------------"
	
	sudo service virtuoso-opensource-6.1 stop
	#adding datasets path to DirsAllowed in virtuoso.ini
	sudo sed -i "s~DirsAllowed              = ~DirsAllowed              = $root/data/DsAndOnt, $root/data/summaries, ~g" /etc/virtuoso-opensource-6.1/virtuoso.ini
	#setting virtuoso db files directory
	#sudo sed -i "s~/var/lib/virtuoso-opensource-6.1/db/~$root/data/triple-store/~g" /etc/virtuoso-opensource-6.1/virtuoso.ini
	sudo service virtuoso-opensource-6.1 start
}


case "$1" in
        start)
			start_enviroment 
			start 
            ;;
        start_enviroment)
			start_enviroment 
            ;;
        initMongo)
			initMongo 
            ;;
		build)
			build
			;;
		install)
			install $2
			;;
		config)
			config $2 $3
			;;
        *)
        	echo "Usage: abstat start | start_enviroment | build | install | config"
			;;
esac
