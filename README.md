# ABSTAT 


## Prerequisites for running ABSTAT
Tested on Ubuntu 16.04 LTS
ABSTAT depends on the following software:
```
* Nginx
* MongoDB
* Java
* Maven
```

## Checking out the repository, install and test ABSTAT
```
#!bash
$ git clone https://bitbucket.org/disco_unimib/abstat
$ cd abstat
$ sudo ./abstat.sh install # if you do not have the software requirements. If you are running abstat on a cluster include the -distributed option
$ sudo ./abstat.sh config nginx_port application_port # note that previous Nginx configuration will be overwritten
$ sudo ./build-and-test.sh 
```

## Controlling ABSTAT

ABSTAT can be controlled using the script ```abstat.sh``` from the root of the repository with the following commands:
```
$ sudo abstat.sh build # builds ABSTAT 
```
```
$ sudo abstat.sh start # starts ABSTAT
```

## License

GNU Affero General Public License v3.010
